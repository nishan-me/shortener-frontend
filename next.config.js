module.exports = {
  env: {
    API_URL: process.env.API_URL,
    API_TIME_OUT: process.env.API_TIME_OUT,
  },
};
