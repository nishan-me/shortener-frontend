import React from 'react';
import { Provider } from 'react-redux';
import { useStore } from 'store/index';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/global.scss';

function Shortner({ Component, pageProps }) {

  /**
   * Initial Redux state
   */
   const store = useStore(pageProps.initialReduxState);
   
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

Shortner.propTypes = {
  Component: PropTypes.func,
  pageProps: PropTypes.object,
};

export default Shortner;
