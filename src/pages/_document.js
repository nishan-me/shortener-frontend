import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class DocumentTemplate extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <link
            rel="apple-touch-icon-precomposed"
            sizes="57x57"
            href="/favicon/apple-touch-icon-57x57-precomposed.png"
          />
          <link
            rel="apple-touch-icon-precomposed"
            sizes="72x72"
            href="/favicon/apple-touch-icon-72x72-precomposed.png"
          />
          <link
            rel="apple-touch-icon-precomposed"
            sizes="114x114"
            href="/favicon/apple-touch-icon-114x114-precomposed.png"
          />
          <link
            rel="apple-touch-icon-precomposed"
            sizes="144x144"
            href="/favicon/apple-touch-icon-144x144-precomposed.png"
          />
          <link rel="shortcut icon" href="/favicon.ico" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default DocumentTemplate;
