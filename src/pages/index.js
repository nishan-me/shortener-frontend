import React from 'react';
import Homepage from 'layouts/static/home';

export default function Home() {
  return <Homepage />;
}
