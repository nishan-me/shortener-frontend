import axios from 'axios';
import { env } from '../../../next.config';

export const LONG_URL_SUMITTED = 'LONG_URL_SUMITTED';
export const SHORT_URL_RECIEVED = 'SHORT_URL_RECIEVED';
export const SHORT_URL_ERROR = 'SHORT_URL_ERROR';

export const getShortenedUrl = ({ longUrl }) => (dispatch) => {
  dispatch({
    type: LONG_URL_SUMITTED,
  });
  axios
    .post(`${env.API_URL}/shorten`, { long_url: longUrl })
    .then((response) => {
      const { data } = response.data;
      dispatch({
        type: SHORT_URL_RECIEVED,
        payload: { ...data },
      });
    })
    .catch((error) => {
      dispatch({
        type: SHORT_URL_ERROR
      });
    });
};
