import { initialState as shortenUrlInitialState } from './shorten_url_reducer';

const initialState = {
  shortenUrl: shortenUrlInitialState,
};

export default initialState;
