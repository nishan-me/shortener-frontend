import { combineReducers } from 'redux';
import { default as shortenUrl } from './shorten_url_reducer';

const appReducer = combineReducers({
  shortenUrl,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
