import {
  LONG_URL_SUMITTED,
  SHORT_URL_RECIEVED,
  SHORT_URL_ERROR,
} from 'store/actions/shorten_action';

export const initialState = {
  isLoading: false,
  longUrl: null,
  shortUrl: null,
  slug: null,
  errors: {},
};

/**
 * Shorten URL data
 * @param state
 * @param action
 */
const shortenUrlReducer = (state = initialState, action) => {
  switch (action.type) {
    case LONG_URL_SUMITTED:
      return {
        ...state,
        isLoading: true,
      };
    case SHORT_URL_RECIEVED:
      const { short_url, long_url, slug } = action.payload;
      return {
        ...state,
        isLoading: false,
        shortUrl: short_url,
        longUrl: long_url,
        slug
      };
    case SHORT_URL_ERROR:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return { ...state };
  }
};

export default shortenUrlReducer;
