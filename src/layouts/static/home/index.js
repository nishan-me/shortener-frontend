import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Head from 'next/head';
import styles from './Home.module.scss';
import { useForm } from 'react-hook-form';
import { getShortenedUrl } from '../../../store/actions/shorten_action';

export default function Home() {
  const dispatch = useDispatch();

  const { register, handleSubmit } = useForm();

  const shortenUrlState = useSelector((state) => state.shortenUrl);

  const [shortUrl, setShortUrl] = useState('');

  useEffect(() => {
    console.log(shortUrl);
    setShortUrl(shortenUrlState.shortUrl)
  }, [shortenUrlState]);

  const onSubmit = (data) => dispatch(getShortenedUrl(data));

  return (
    <div>
      <Head>
        <title>URL Shortner</title>
      </Head>

      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">URL Shortener</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Enter the long URL you want to shorten below and get an instant
            short URL :-)
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="input-group">
                <input
                  type="text"
                  {...register('longUrl', { required: true })}
                  className="form-control rounded"
                  placeholder="Shorten your link"
                  aria-label="Shorten"
                  required
                />
                <input
                  type="submit"
                  className="btn btn-primary"
                  value="Shorten"
                />
              </div>
            </form>
          </div>
          <div className="d-grid gap-2 d-md-flex justify-content-sm-center my-3">
            <div><a href={shortUrl} target="blank">{shortUrl}</a></div>
          </div>
        </div>
      </div>
    </div>
  );
}
